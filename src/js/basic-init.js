$(document).ready(function () {
    'use strict';

    // inserting an icon into a menu link
    if (window.innerWidth > 1200) {
        let hasChildrenLink = document.querySelector('.main-nav .menu-item-has-children > a');
        hasChildrenLink.insertAdjacentHTML('beforeEnd', '<svg xmlns="http://www.w3.org/2000/svg" width="10" height="7" viewBox="0 0 436.68648 265.01053" fill="currentColor"><path d="M201.37276,257.98152,7.02975,63.63751a23.99908,23.99908,0,0,1,0-33.941l22.667-22.667a23.99976,23.99976,0,0,1,33.901-.04l154.746,154.021,154.745-154.021a23.99978,23.99978,0,0,1,33.901.04l22.667,22.667a23.99914,23.99914,0,0,1,0,33.941l-194.342,194.344a24.00209,24.00209,0,0,1-33.942,0Z"/></svg>');
    }

    // slider offer
    let $slider = $('.slider-offer-home');

    if ($slider.length) {
        let currentSlide;
        let slidesCount;
        let sliderCounter = document.createElement('div');
        sliderCounter.classList.add('slider-offer-home__counter');

        let updateSliderCounter = function(slick, currentIndex) {

            currentSlide = slick.slickCurrentSlide() + 1;
            if (currentSlide < 10) {
                currentSlide = '0' + currentSlide;
            }

            slidesCount = slick.slideCount;
            if (slidesCount < 10) {
                slidesCount = '0' + slidesCount;
            }

            $(sliderCounter).html(`
                    <span>${currentSlide}</span>
                    <span>/</span>
                    <span>${slidesCount}</span>`);
        };

        $slider.on('init', function(event, slick) {
            $slider.append(sliderCounter);
            updateSliderCounter(slick);
        });

        $slider.on('afterChange', function(event, slick, currentSlide) {
            updateSliderCounter(slick, currentSlide);
        });
    }

    $slider.slick({
        dots: false,
        arrows: true,
        infinite: true,
        fade: false,
        adaptiveHeight: true,
        // speed: 1800,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        // autoplaySpeed: 3500,
        pauseOnFocus: false,
        pauseOnHover: false,
        prevArrow: '<button class="slider-offer-home__arrow slider-offer-home__arrow--prev" type="button"><span class="visually-hidden">Назад</span><svg xmlns="http://www.w3.org/2000/svg" width="26.158" height="11.014" viewBox="0 0 26.158 11.014" fill="#fff"><path d="M0 5.507l5.507 5.507v-4.13h20.651V4.13H5.507V0z"/></svg></button>',
        nextArrow: '<button class="slider-offer-home__arrow slider-offer-home__arrow--next" type="button"><span class="visually-hidden">Вперед</span><svg xmlns="http://www.w3.org/2000/svg" width="26.158" height="11.014" viewBox="0 0 26.158 11.014" fill="#fff"><path d="M26.158 5.507L20.651 0v4.13H0v2.754h20.651v4.13z"/></svg></button>',
    });

    // slider partners
    $('.slider-partners').slick({
        dots: false,
        arrows: true,
        infinite: true,
        fade: false,
        adaptiveHeight: false,
        // speed: 1800,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        // autoplaySpeed: 3500,
        pauseOnFocus: false,
        pauseOnHover: false,
        prevArrow: '<button class="slider-partners__arrow slider-partners__arrow--prev" type="button"><span class="visually-hidden">Назад</span><svg xmlns="http://www.w3.org/2000/svg" width="26.158" height="11.014" viewBox="0 0 26.158 11.014" fill="#fff"><path d="M0 5.507l5.507 5.507v-4.13h20.651V4.13H5.507V0z"/></svg></button>',
        nextArrow: '<button class="slider-partners__arrow slider-partners__arrow--next" type="button"><span class="visually-hidden">Вперед</span><svg xmlns="http://www.w3.org/2000/svg" width="26.158" height="11.014" viewBox="0 0 26.158 11.014" fill="#fff"><path d="M26.158 5.507L20.651 0v4.13H0v2.754h20.651v4.13z"/></svg></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });

    // slider category
    /*$('.category-slider').slick({
        dots: true,
        arrows: true,
        infinite: true,
        fade: false,
        adaptiveHeight: true,
        // speed: 1800,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: false,
        // autoplaySpeed: 3500,
        pauseOnFocus: false,
        pauseOnHover: false,
        prevArrow: '<button class="category-slider__arrow category-slider__arrow--prev" type="button"><span class="visually-hidden">Назад</span><svg xmlns="http://www.w3.org/2000/svg" width="26.158" height="11.014" viewBox="0 0 26.158 11.014" fill="#000"><path d="M0 5.507l5.507 5.507v-4.13h20.651V4.13H5.507V0z"/></svg></button>',
        nextArrow: '<button class="category-slider__arrow category-slider__arrow--next" type="button"><span class="visually-hidden">Вперед</span><svg xmlns="http://www.w3.org/2000/svg" width="26.158" height="11.014" viewBox="0 0 26.158 11.014" fill="#000"><path d="M26.158 5.507L20.651 0v4.13H0v2.754h20.651v4.13z"/></svg></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
        ]
    });*/

    // get the width of the scrollbar
    document.documentElement.style.setProperty('--scrollbar-width', (window.innerWidth - document.documentElement.clientWidth) + "px");

    // Init Masonry
    var $grid = $('.masonry').masonry({
        itemSelector: '.masonry__item',
        columnWidth: '.masonry__item',
        gutter: 10,
    });

    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });

    // Init CustomScrollbar
    $(".gallery__custom-scrollbar").mCustomScrollbar({
        theme:"dark"
    });

    // init mmenu
    if (window.innerWidth < 1200) {
        $('#js-main-nav').mmenu({
            // wrappers: ["wordpress"],
            extensions: [
                'border-full',
                'fx-menu-slide',
                'fx-listitems-slide',
                'multiline',
                'pagedim-black',
            ],
            "counters": true,
            "setSelected": {
                "hover": true
            },
            navbar: {
                title: 'Меню'
            },
            "navbars": [
                // {
                //     "position": "top",
                //     "type": "tabs",
                //     "content": [
                //         "<a href='#categories'>Categories</a>",
                //         "<a href='#service'>Our Service</a>"
                //     ]
                // },
                // {
                //     "position": "bottom",
                //     "content": [
                //         '<ul class="top-nav__social social-top-nav mm-tileview">' +
                //         '    <li class="social-top-nav__item">' +
                //         '        <a class="social-top-nav__link social-top-nav__link--facebook" href="' + 5 + '" title="Our Facebook group" target="_blank">' +
                //         '            <span class="visually-hidden">Our Facebook group</span>' +
                //         '         </a>' +
                //         '     </li>' +
                //         '    <li class="social-top-nav__item">' +
                //         '        <a class="social-top-nav__link social-top-nav__link--instagram" href="' + 5 + '" title="Our Instagram page" target="_blank">' +
                //         '            <span class="visually-hidden">Our Instagram page</span>' +
                //         '        </a>' +
                //         '    </li>' +
                //         '</ul><!-- / .social-top-nav -->'
                //     ]
                // }
            ],
        });
    }

    // menu stuff
    let mainHeaderBottom = document.querySelector('.main-header__bottom') || '';

    if (mainHeaderBottom) {
        // sticky menu
        let mainHeaderBottomWrapper = document.querySelector('.main-header__bottom-wr');

        let waypointNav = new Waypoint({
            element: mainHeaderBottom,
            handler: function (direction) {
                if (direction === 'down') {
                    mainHeaderBottomWrapper.classList.add('main-header__bottom-wr--sticky');
                    mainHeaderBottom.style.height = mainHeaderBottomWrapper.offsetHeight + 'px';
                } else {
                    mainHeaderBottomWrapper.classList.remove('main-header__bottom-wr--sticky');
                    mainHeaderBottom.style.height = 'auto';
                }
            },
            offset: -1
        });
    }

    // show/hide mobile search
    $('.main-header__search-btn-mobile').on('click', function () {
        $('.main-header__bottom-search').slideToggle(300);
    });

    ////////////////////////////////////////////////////////////////////////////
    // Send callback / Send request / Buy product

    $('[data-submit]').on('click', function(e) {
        e.preventDefault();
        $(this).parent('form').submit();
    });

    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            let re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Пожалуйста, проверьте свои данные"
    );

    function valEl(el) {
        let validator = el.validate({
            rules:{
                'user[name]': {
                    required: true
                },
                'user[email]': {
                    required: true
                },
                'user[phone]': {
                    required: true,
                    regex: '^([\+]+)*[0-9\x20\x28\x29\-]{5,20}$'
                }
            },
            messages:{
                'user[name]': {
                    required: ''
                },
                'user[email]': {
                    required: '',
                    email: ''
                },
                'user[phone]': {
                    required: '',
                    regex: ''
                },
            },
            submitHandler: function (form) {
                $.fancybox.close();
                $('.loader').fadeIn();

                $.ajax({
                    url: `${templateUrl}/tpl-sys-request.php`,
                    type: 'POST',
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    beforeSend: function(){

                    },
                    success: function (data) {
                        setTimeout(function () {
                            $('.loader').fadeOut();
                        },800);

                        setTimeout(function () {
                            $.fancybox.open({
                                src: '.modal-thanks',
                                type: 'inline',
                                touch: false,
                                backFocus: false
                            });
                        },1100);

                        jQuery(form).find(".form__field").each(function(){ jQuery(this).val(''); });
                    }
                });

                return false;
            }
        });
    }

    $('.js-form').each(function() {
        valEl( $(this) );
    });

    ////////////////////////////////////////////////////////////////////////////

    // masked input
    $('input[type="tel"]').mask("+7 (999) 999-99-99");

    // is mobile
    function isMobile() {
        return $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
    }

}); // end ready
