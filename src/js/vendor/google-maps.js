function initMap() {

    let myMap;
    let element = document.getElementById('google-map');

    let options = {
        zoom: 17,
        center: {lat:51.191427, lng:71.438088},
        // scrollwheel: false
    };

    // maps initialization
    myMap = new google.maps.Map(element, options);

    // marker and info-window
    let marker = new google.maps.Marker({
        position: {lat:51.191427, lng:71.438088},
        map: myMap,
        animation: google.maps.Animation.DROP
    });

    let InfoWindow = new google.maps.InfoWindow({
        content: `<div class="info-window">
                    <img src="img/base/google-map-marker-img.jpg" width="150" alt="">
                    <strong>Конкрит Продактс Астана, ТОО</strong>
                    <p>Поставщик стройматериалов</p>
                    <a href="tel:+77015280456">+7 701 528 04 56</a>
                  </div>`
    });

    marker.addListener('click', function () {
        InfoWindow.open(myMap, marker);
    });
}
